## Interface: 80000
## Title: Titan Panel [|cffeda55fRest+|r] |cff00aa008.0.0.0|r
## Notes: Keeps track of the RestXP amounts and status for all of your characters. Is it updated to run on patch 8.x.
## Author: Kernighan
## Author: GrayElf, Maillen, Vogonjeltz, Madysen
## DefaultState: Enabled
## SavedVariables: RestPlus_Data, RestPlus_Settings, RestPlus_Colors
## OptionalDeps:
## Dependencies: Titan
## Version: 8.0.0.0
## X-Category: Interface Enhancements
## X-Date: 2018-07-26
## X-Child-of: Titan
## X-Curse-Packaged-Version: 8.0.0.0
## X-Curse-Project-Name: Titan Panel [RestPlus]
## X-Curse-Project-ID: rest_plus
## X-Curse-Repository-ID: wow/rest_plus/mainline
TitanRestPlus.xml
